google.load('visualization', '1', {
    packages: ['corechart', 'line']
});

google.setOnLoadCallback(function() {

    try {
        var socket = io.connect('http://127.0.0.1:8080');
    } catch(e) {
        console.log(e);
    }

    var arrayN=[];
    arrayN.push(['Time', 'Random Number']);

    if(socket !== undefined) {
         socket.on('data', function(update) {
             console.log([update.date, update.random]);
             arrayN.push([update.date, update.random]);
         });

            setInterval(function() {

                var datachart = google.visualization.arrayToDataTable(arrayN);

                var view = new google.visualization.DataView(datachart);


                var options = {
                    title: 'Random Number Vs Time',
                    width: 800,
                    height: 600,
                    bar: {groupWidth: '95%'},
                    legend: {position: 'right'},
                    hAxis: {title: 'Time'},
                    vAxis: {title: 'Random Number'}
                };

                var chart = new google.visualization.LineChart(document.getElementById('spectrum'));
                chart.draw(view, options);
            }, 1000);
    }

});
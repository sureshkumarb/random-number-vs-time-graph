// CALL THE PACKAGES----------------

var express = require('express');
var mongo = require('mongodb');
var socketio = require('socket.io');
var config = require('./config');
var path = require('path');
var morgan = require('morgan');

// START THE SERVER
// ==================================

var app = express();
var mognodb = mongo.MongoClient;
var client = socketio.listen(app.listen(config.port)).sockets;
console.log('Magic happens on port ' + config.port);

app.use(morgan('dev'));


// set the public folder to server public assets.
app.use(express.static(__dirname + '/public'));

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/views/index.html'));
});

// Connect to mongodb.

mognodb.connect(config.database, function(err, db) {

    if(err) throw  err;

    // make socket connection.

    client.on('connection', function(socket) {

        var col = db.collection('collection');

        var minutes = 1;
        var interval = minutes * 60 * 1000;

        col.drop();

        var date = new Date();
        var random = Math.floor(Math.random() * 100) + 1;
        var dateFormat = [date.getDate(), date.getMonth()+1, date.getFullYear()].join('/')
            + ' ' +
            [date.getHours(),date.getMinutes(), date.getSeconds()].join(':');

        var data = {
            date: dateFormat,
            random: random
        };

        socket.emit('data', data);

        col.insertOne(data, function(err) {
            if(err) throw err;
        });


        // Updating Database for every one minute.

        var count = 0;

        setInterval(function() {

            var date = new Date();
            var random = Math.floor(Math.random() * 100) + 1;
            var dateFormat = [date.getDate(), date.getMonth()+1, date.getFullYear()].join('/')
                + ' ' +
                [date.getHours(),date.getMinutes(), date.getSeconds()].join(':');

            var data = {
                date: dateFormat,
                random: random
            };

            col.insertOne(data, function (err) {
                    if (err) throw err;
                    socket.emit('data', data);
                    console.log('inserted data' + count++);
                });
        }, interval);

    });

});